<!DOCTYPE html>
<html>
	<head>
		<?php
			$url_pdf = $_GET['url_pdf'];
			$url_fav = $_GET['url_fav']; 
			$nombre  = $_GET['nombre']; 
			$id_pdf  = $_GET['id_pdf'];
		?>
		<script type="text/javascript">
			var url_pdf = {"key":"<?php echo $url_pdf;  ?>"}
		</script>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Practicas Clínicas</title>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<link rel="stylesheet" type="text/css" href="css/practicas.css">
		<script src="js/jquery-1.11.2.min.js"></script>		
		<script src="js/lib/pdf.js"></script>
		<script src="bower_components/angular/angular.js"></script>
		<script src="bower_components/angular-pdf/dist/angular-pdf.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/main.js"></script>
		<script src="js/practicas.js"></script>	
	</head>
	<body>
	    <style type="text/css">
	    	#btn-menu-mob{
				display: none !important;
			}
	    </style>
		<header>
			<div id="top-blue-line"></div>			
			<div class="container">
				<div class="row">
					<div id="logo"><img src="img/logo.png" style="max-height:40px;margin-left:5%"></div>
					<div id="portal-name-pdf"><i>Portafolio Digital</i></div>
					<div id="btn-menu-mob">&#9776;</div>
					<div id="btn-search-mob"></div>
				</div>
			</div>
			<div id="controls" class="controls">
				<p class="container titulo_vista">
					<?php echo $nombre; ?>
				</p>
				<DIV id="btn_fav_agrega" class="btn-favoritos-check" onclick="document.getElementById('save_msg').style.display = 'block'; 
																			  document.getElementById('btn_fav_agrega').style.display = 'none';">
			        <form method="POST" id="form-fav">
							<p style="margin-top:-15px;">
								FAVORITOS
							</p>
							<input type='submit' class='fav_pdf'/>
							<input type='hidden' name='id_pdf' id='id_pdf' value='<?php echo $id_pdf; ?>'>
			        </form>
				</DIV>
				<DIV id="btn_fav_elimina" class="btn-favoritos-check" onclick="document.getElementById('save_msg').style.display = 'block';
																			   document.getElementById('btn_fav_elimina').style.display = 'none';">
			        <form method="POST" id="form-fav-delete">
							<p style="margin-top:-15px;">
								ELIMINAR DE FAVORITOS
							</p>
							<input type='submit' class='fav_pdf'/>
							<input type='hidden' name='id_pdf' id='id_pdf' value='<?php echo $id_pdf; ?>'>
			        </form>
				</DIV>
				<DIV id="save_msg" class="container-fluid" style="display:none;position:relative;float:right;margin-top:-40px;margin-right:100px">
			        <p>
			        	<I>CARGANDO...</I>
			        </p>
				</DIV>
			</div>
		</header>	
		<div class="container pdf_controls_components" ng-app="app">
			
			<div class="row" ng-controller="pdfCtrl">

				<div class="col-md-12">
				  	<ng-pdf template-url="partials/viewer.html" canvasid="pdf" scale="1.5"></ng-pdf>

				</div>

			</div>

		</div>
	</body>
    <script type="text/javascript">
    	fav_pdf_valid(<?php echo $id_pdf; ?>);
    </script>
	<script src="js/app.js"></script>
</html>